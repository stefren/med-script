const request = require('request');
let opt = {
    url: 'http://sms-api.luosimao.com/v1/send.json',
    method: 'POST',
    auth: {
        username: 'api',
        password: 'key-a8ff56f82145859dd461934e0c0275c7'
    },
    rejectUnauthorized: false,
    agent: false,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
    },
    form: {
        mobile: '',
        message: ''
    }
};
let sms = function (mobile, message) {
    opt.form.mobile = mobile;
    opt.form.message = message;
    return new Promise(function (resolve, reject) {
        request(opt, function (err, resp, body) {
            if (err) reject(err);
            resolve(JSON.parse(body));
        })
    })
};

module.exports = sms;