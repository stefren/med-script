/* xlsx template generator
   usage: node xlsx_generator path/to/input.json output_name
*/
const fs = require('fs');
const xlsx = require('node-xlsx');
const _ = require('lodash');
const moment = require('moment');
const request = require('request');

let output_name = process.argv[2];
//input json sample
let sample_json1 = {
    basic_stat: {
        total_reg: 42,
        total_attend: 42,
        mod_count: 42,
        abort_count: 0,
        submit_count: 42,
        upper_count: 20
    },
    details: [
        {
            id: 0,
            account: 'fooaccount',
            name: 'handsome',
            email: 'foo@bar.com',
            mobile: '188888888',
            score: 99,
            tour_title: 'foo game',
            submit_time: '2049/07/01'
        },
        {
            id: 1,
            account: 'fooaccount',
            name: 'handsome',
            email: 'foo@bar.com',
            mobile: '188888888',
            score: 99,
            tour_title: 'foo game',
            submit_time: '2049/07/01'
        },
        {
            id: 2,
            account: 'fooaccount',
            name: 'handsome',
            email: 'foo@bar.com',
            mobile: '188888888',
            score: 99,
            tour_title: 'foo game',
            submit_time: '2049/07/01'
        }
    ]
}

request({
    url: 'https://transgod.cn/mis/setting/com_data',
    method: 'get',
    headers: {
        'Cookie': 'intercom-lou-axmr1tti=1; intercom-id-axmr1tti=516fb4a4-bd1b-4158-b724-7affd440f6a6; connect.sid=s%3AtX42XyK5VQvqkJy9Ayn4NZ8Pn6ITAZWs.ognYTZzOp0S5VxXMTuTi5pbLmsNN0KEQM0bOw4QbKCI; sid=tX42XyK5VQvqkJy9Ayn4NZ8Pn6ITAZWs; intercom-session-axmr1tti=Yk8yaWFXNmJiRTc1U0hEd1ZmbGJUK0w2UHEwMy9JT1JZKzRTc3BXZ0trSzNtKzFmbGpFK3c0ckhkVlRpZHhVVy0tRHZ4WVBkN0M0ODBiWFRnbWh0NHlaZz09--e9ae949a90a1fb70ad568951e8abaf33acc53e70'
    }
}, function (err, resp, body) {
    let sample_json = JSON.parse(body).data;
    let data = xlsx.parse('./xlsx/output_tpl.xlsx');

    let main_arr = data[0].data.slice(0,9);
    data[0].data = main_arr;
//fill time
    let gen_time = moment().format('YYYY/MM/DD hh:mm:ss A');
    _.fill(data[0].data[1],gen_time, 1);
//fill basic statistics
    let stats = _.concat([], sample_json.basic_stat.total_reg, sample_json.basic_stat.total_attend, sample_json.basic_stat.mod_count, sample_json.basic_stat.abort_count, sample_json.basic_stat.submit_count, sample_json.basic_stat.upper_count);
    data[0].data.splice(5, 1, stats);
//fill details
    for(let i=0; i<sample_json.details.length; i++){
        let detail = sample_json.details[i];
        let detail_row = _.concat([], detail.id, detail.account, detail.name, detail.email, detail.mobile.toString(), detail.score, detail.tour_title, detail.submit_time);
        data[0].data.push(detail_row);
    }
    let buffer = xlsx.build(data);

    fs.writeFileSync('./xlsx/'+output_name+'.xlsx', buffer);
})
