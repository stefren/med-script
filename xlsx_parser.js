const xlsx = require('node-xlsx');
const file_input = process.argv[2];
const fs = require('fs');

console.log(process.argv);

//parse xlsx to json
let work_sheets = xlsx.parse(file_input);
work_sheets.forEach(function (sheet) {
    let data = sheet.data;
    let arr = [];
    for(let i=1; i<data.length; i++){
        let json = {};
        json.id = data[i][0];
        json.submit_time = data[i][1];
        json.name = data[i][2];
        json.mobile = data[i][3];
        arr.push(json)
    }
    let output_json = {
        name: sheet.name,
        data: arr
    };
    fs.writeFileSync('./json/'+sheet.name, JSON.stringify(output_json));
});
