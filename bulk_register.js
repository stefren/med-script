const request = require('request');
const json_input = process.argv[2];
const fs = require('fs');
const sms = require('./sms');
const ali = require('./ali_sms');

let list = fs.readFileSync(json_input);

//bulk register
let players = JSON.parse(list).data;

function get_token() {
    return new Promise(function (resolve, reject) {
       request({
           url: 'https://stage.transgod.cn/ucenter/authentication',
           method: 'POST',
           form: {
               name: 'arya',
               pass: 'stark'
           }
       }, function (err, resp, body) {
           if (err) resolve('');
           let result = JSON.parse(body);
           if (result.error != 0) {
               resolve('')
           } else {
               resolve(result.token);
           }
       })
    })
}

players.forEach(async function (player) {
    let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYXJ5YSIsInBhc3MiOiJzdGFyayIsImlhdCI6MTUyNzczMjU3MywiZXhwIjoxNTI3ODE4OTczfQ.Saxo4VyfEEZW64JnsgN7_pKhNk3aZdzK0-skiF6hNZc';
    request({
        url: 'https://transgod.cn/ucenter/mobile/reguser',        //正式环境需要改成transgod.cn域名
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': token
        },
        form: {
            user_name: player.name,
            full_name: player.name,
            password: '12345678',
            mobile: player.mobile
        }
    }, async function (err, resp, body) {
        if (err) {
            console.log('auto register failed! do it manually:'+JSON.stringify(player));
        }
        let result = JSON.parse(body);
        if (result.error && result.error == 1) {
            console.log('auto register failed! do it manually:'+JSON.stringify(player));
        } else {
            console.log('auto register success!'+JSON.stringify(player)+body);
            if (result.error == 2 && result.race_flag == 0) {
                let con = await ali(player.mobile, 'SMS_136166103');
                console.log('con',con);
            } else {
                if (result.error == 0) {
                    try {
                        let con = await ali(player.mobile, 'SMS_136176139')
                        if (con.Message == 'OK') {
                            ali(player.mobile, 'SMS_136166103')
                        }
                    } catch (err) {
                       console.log('sms err:',player);
                    }
                }
            }
        }
    })
});
