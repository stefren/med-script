自动注册及xlsx导出脚本使用方法
------------
报名表读取
<pre>$ node xlsx_parser /path/to/input.xlsx</pre>
在项目json文件夹中将，导入xlsx的每个sheet将会生成一个序列化json文件，方便下一步批量注册使用
批量注册
<pre>$ node bulk_register.js /path/to/input_json</pre>
导入上一步生成的序列化json，将自动注册报名表中用户。用户名为手机号，初始密码123456
模板导出
<pre>$ node xlsx_generator.js /path/to/input_json output_filename</pre>
导入序列化的json模板，在xlsx文件夹里生成名为output_filename的xlsx文件